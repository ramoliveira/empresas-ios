//
//  NetworkTests.swift
//  empresas-iosTests
//
//  Created by Ramon Dias de Oliveira de Almeida on 01/01/21.
//

import XCTest
import empresas_ios

class NetworkTests: XCTestCase {
    func testNetwork_authetication_withoutNetworkStructure() {
        guard let url = URL(string: "https://empresas.ioasys.com.br/api/v1/users/auth/sign_in") else {
            XCTFail("Expected non-nil URL")
            return
        }
        
        struct Body: Codable {
            var email: String
            var password: String
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let body = Body(email: "testeapple@ioasys.com.br", password: "12341234")
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        if let requestBody = try? JSONEncoder().encode(body) {
            request.httpBody = requestBody
        } else {
            XCTFail("Failed to Encode Body")
        }
        
        let expectation = XCTestExpectation(description: "Post data to Authentication")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                XCTFail("Failed with Error")
                expectation.fulfill()
            }
            
            guard let response = response as? HTTPURLResponse else {
                XCTFail("Failed to Invalid Response")
                expectation.fulfill()
                return
            }
            
            if 200...299 ~= response.statusCode {
                if let data = data {
                    do {
                        let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
                        XCTAssertTrue(!dict.isEmpty)
                        expectation.fulfill()
                    } catch {
                        XCTFail("Failed to Failed to Decode")
                        expectation.fulfill()
                    }
                } else {
                    XCTFail("Failed, invalid data")
                    expectation.fulfill()
                }
            } else {
                XCTFail("Failed, Server Error")
                expectation.fulfill()
            }
        }
        task.resume()
        
        wait(for: [expectation], timeout: 20.0)
    }
    
    
    func testNetwork_post_withNetworkStructure() {
        let user = User(email: "testeapple@ioasys.com.br", password: "12341234")

        let expectation = XCTestExpectation(description: "Post data to Authentication")
        
        guard let url = URL(string: "https://empresas.ioasys.com.br/api/v1/users/auth/sign_in") else {
            XCTFail("Expected non-nil URL")
            return
        }

        Network.post(url: url, body: user, ofType: User.self, header: [:]) { result in
            switch result {
            case .failure(let error):
                XCTFail("\n\n===> Failed with error:\n\(error) <===\n\n")
                expectation.fulfill()
            case .success(let data):
                XCTAssertTrue(!data.header.isEmpty && !data.body.isEmpty)
                expectation.fulfill()
            }
        }

        wait(for: [expectation], timeout: 20.0)
    }
    
    func testNetwork_get_withNetworkStructure() {
        enum EnterprisesURL: String {
            case enterpriseIndex = "https://empresas.ioasys.com.br/api/v1/enterprises?enterprise_types=1&name=aQm"
            case show = "https://empresas.ioasys.com.br/api/v1/enterprises/1"
        }
        
        guard let url = URL(string: EnterprisesURL.show.rawValue) else {
            XCTFail("Expected non-nil URL")
            return
        }
        
        let header = [
            "access-token":"w22Qw7CzbuUQ99Mtcoyg0Q",
            "client":"BtuWJ2NWhkVSwPtRIM3atg",
            "uid":"testeapple@ioasys.com.br"
        ]
        
        let expectation = XCTestExpectation(description: "Get from ptsv2")

        Network.get(url: url, header: header) { result in
            switch result {
            case .failure(let error):
                XCTFail("\n\n===> Failed with error:\n\(error) <===\n\n")
                expectation.fulfill()
            case .success(let data):
                XCTAssertTrue(!data.header.isEmpty && !data.body.isEmpty)
                expectation.fulfill()
            }
        }

        wait(for: [expectation], timeout: 20.0)
    }
}
