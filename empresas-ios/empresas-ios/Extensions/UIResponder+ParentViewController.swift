//
//  ViewInput.swift
//  empresas-ios
//
//  Created by Ramon Dias de Oliveira de Almeida on 31/12/20.
//

import Foundation
import UIKit

extension UIResponder {
    public var parentViewController: UIViewController? {
        return next as? UIViewController ?? next?.parentViewController
    }
}
