//
//  UITableView+RegisteredNibsAndClasses.swift
//  empresas-ios
//
//  Created by Ramon Dias de Oliveira de Almeida on 04/01/21.
//

import Foundation
import UIKit

extension UITableView {
    var registeredNibs: [String: UINib] {
        let dict = value(forKey: "_nibMap") as? [String: UINib]
        return dict ?? [:]
    }
    
    var registeredClasses: [String: Any] {
        let dict = value(forKey: "_cellClassDict") as? [String: Any]
        return dict ?? [:]
    }
    
    var count: Int {
        let sections = self.numberOfSections
        var rows = 0
        
        for i in 0...sections-1 {
            rows += self.numberOfRows(inSection: i)
        }
        
        return rows
    }
}
