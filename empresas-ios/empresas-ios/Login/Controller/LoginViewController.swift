//
//  ViewController.swift
//  empresas-ios
//
//  Created by Ramon Dias de Oliveira de Almeida on 30/12/20.
//

import UIKit

public class LoginViewController: UIViewController {
    lazy var model: LoginModel = {
       return LoginModel(controller: self)
    }()

    public var loginView: LoginView! {
        guard isViewLoaded else { return nil }
        let loginView = view as! LoginView
        return loginView
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.loginView.setupView()
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        self.loginView.renderStart()
    }
}

extension LoginViewController {
    public func login(with user: User) {
        DispatchQueue.main.async {
            self.loginView.renderLoading()
        }
        self.model.requestLogin(with: user)
    }
    
    public func successfulLogin() {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: SeguesId.home.rawValue, sender: self)
        }
    }
    
    public func failedLogin() {
        self.loginView.renderError()
    }
    
    public func renderLoading() {
        
    }
}

