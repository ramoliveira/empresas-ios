//
//  LoginView.swift
//  empresas-ios
//
//  Created by Ramon Dias de Oliveira de Almeida on 31/12/20.
//

import UIKit

/// View of LoginViewController
public class LoginView: UIView {
    //MARK: - IBOutlets -
    
    /* This section contains every IBOutlet used in LoginView */
    
    //MARK: - Header -
    @IBOutlet weak var headerBackgroundImageView: UIImageView!
    @IBOutlet weak var headerLabel: UILabel!
    
    //MARK: - Login Input -
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailInvalidImageView: UIImageView!
    @IBOutlet weak var emailInputBackground: UIView!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordInvalidImageView: UIImageView!
    @IBOutlet weak var passwordInputBackground: UIView!
    @IBOutlet weak var passwordVisibleButton: UIButton!
    @IBOutlet weak var invalidCredentialsLabel: UILabel!
    
    //MARK: - Login Button -
    @IBOutlet weak var loginButton: UIButton!
    
    public var controller: LoginViewController? {
        get {
            return self.parentViewController as? LoginViewController
        }
    }
    
    //MARK: - IBActions -
    @IBAction func changePasswordVisibility(_ sender: UIButton) {
        self.passwordTextField.isSecureTextEntry = !self.passwordTextField.isSecureTextEntry
    }
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        if let email = self.emailTextField.text, let password = self.passwordTextField.text {
            let user = User(email: email, password: password)
            self.controller?.login(with: user)
        }
    }
}

extension LoginView {
    public func setupView() {
        // TextFields
        self.emailTextField.layer.cornerRadius = 4
        self.emailTextField.clipsToBounds = true
        
        self.passwordTextField.layer.cornerRadius = 4
        self.passwordTextField.clipsToBounds = true
        
        // Buttons
        self.loginButton.layer.cornerRadius = 4
        self.loginButton.clipsToBounds = true
    }
    
    public func renderStart() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(5)) {
            UIView.animate(withDuration: 5, delay: 10, options: .curveEaseInOut) {
                self.headerBackgroundImageView.image = UIImage(named: "Login Header")
                self.headerLabel.removeFromSuperview()
            } completion: { _ in
                UIView.animate(withDuration: 5) {
                    self.updateConstraintsIfNeeded()
                }
            }
        }
    }
    
    public func renderLoading() {
        print("loading")
    }
    
    public func renderError() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 1) {
                self.passwordVisibleButton.isHidden = true
                
                self.emailInputBackground.layer.borderWidth = 1
                self.emailInputBackground.layer.borderColor = UIColor.red.cgColor
                
                self.emailInvalidImageView.isHidden = false
                
                self.passwordInputBackground.layer.borderWidth = 1
                self.passwordInputBackground.layer.borderColor = UIColor.red.cgColor
                
                self.passwordInvalidImageView.isHidden = false
                
                self.invalidCredentialsLabel.textColor = .red
            }
        }
    }
}
