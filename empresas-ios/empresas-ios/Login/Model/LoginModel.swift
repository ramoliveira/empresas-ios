//
//  LoginModel.swift
//  empresas-ios
//
//  Created by Ramon Dias de Oliveira de Almeida on 03/01/21.
//

import Foundation

public struct LoginModel {
    weak var controller: LoginViewController?
    
    init(controller: LoginViewController) {
        self.controller = controller
    }
    
    public func requestLogin(with user: User) {
        if let url = Paths.castToUrl(urlString: Paths.login) {
            Network.post(url: url, body: user, ofType: User.self, header: [:]) { result in
                switch result {
                case .failure(_):
                    self.controller?.failedLogin()
                case .success(let data):
                    self.saveLogin(info: data)
                    self.controller?.successfulLogin()
                }
            }
        }
    }
    
    public func saveLogin(info: NetworkResponse<[AnyHashable:Any]>) {
        if let accessToken = info.header[Keys.accessToken.rawValue] as? String {
            Storage<String>.save(key: Keys.accessToken.rawValue, value: accessToken)
        }
        if let client = info.header[Keys.client.rawValue] as? String {
            Storage<String>.save(key: Keys.client.rawValue, value: client)
        }
        if let uid = info.header[Keys.uid.rawValue] as? String {
            Storage<String>.save(key: Keys.uid.rawValue, value: uid)
        }
    }
}
