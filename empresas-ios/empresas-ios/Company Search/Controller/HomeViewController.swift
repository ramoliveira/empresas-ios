//
//  HomeViewController.swift
//  empresas-ios
//
//  Created by Ramon Dias de Oliveira de Almeida on 03/01/21.
//

import UIKit

public class HomeViewController: UIViewController {
    lazy var model: HomeModel = {
        return HomeModel(controller: self)
    }()
    
    public var companies: [Company] = [] {
        didSet {
            DispatchQueue.main.async {
                self.homeView.renderData()
            }
        }
    }
    
    public var homeView: HomeView! {
        guard isViewLoaded else { return nil }
        let homeView = view as! HomeView
        return homeView
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        self.homeView.setupView()
    }
}

extension HomeViewController {
    public func presentDetailsOf(company: Company) {
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        let controller = storyboard.instantiateViewController(identifier: "Detail View Controller") as! DetailViewController
        controller.company = company
        self.present(controller, animated: true, completion: nil)
        
    }
    
    public func requestListOfCompanies(with name: String) {
        self.model.requestListOfCompanies(with: name)
    }
    
    public func failedRequest() {
        print("failed")
    }
    
    public func successfulRequest(companies: [Company]) {
        self.companies = companies
    }
}
