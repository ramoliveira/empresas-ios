//
//  HomeTableViewCell.swift
//  empresas-ios
//
//  Created by Ramon Dias de Oliveira de Almeida on 03/01/21.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var companyPictureImageView: UIImageView!
    @IBOutlet weak var cellBackgroundView: UIView!
    
    public static var identifier: String {
        return "HomeTableViewCell"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let margins = UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 0)
        self.contentView.frame = contentView.frame.inset(by: margins)
    }
    
    func setupCell(name: String, photo: UIImage?) {
        self.contentView.layer.cornerRadius = 4
        self.contentView.clipsToBounds = true
        
        self.companyNameLabel.text = name
        
        if let photo = photo {
            self.companyPictureImageView.isHidden = false
            self.companyPictureImageView.image = photo
        }
    }
    
}
