//
//  HomeView.swift
//  empresas-ios
//
//  Created by Ramon Dias de Oliveira de Almeida on 03/01/21.
//

import UIKit

public class HomeView: UIView {

    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var homeSearchBar: UISearchBar!
    @IBOutlet weak var companyResultCountLabel: UILabel!
    @IBOutlet weak var homeTableView: UITableView!
    
    public var controller: HomeViewController? {
        get {
            return self.parentViewController as? HomeViewController
        }
    }
}

extension HomeView {
    public func setupView() {
        self.homeSearchBar.delegate = self
        self.homeSearchBar.searchBarStyle = .default
        self.homeSearchBar.barTintColor = UIColor(red: 0.96, green: 0.96, blue: 0.96, alpha: 1)
        self.homeSearchBar.backgroundColor = UIColor(red: 0.96, green: 0.96, blue: 0.96, alpha: 1)
        self.homeSearchBar.backgroundImage = UIImage()
        self.homeSearchBar.layer.cornerRadius = 4
        self.homeSearchBar.clipsToBounds = true
        
        self.homeTableView.delegate = self
        self.homeTableView.dataSource = self
        self.homeTableView.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: HomeTableViewCell.identifier)
    }
}

extension HomeView {
    public func requestListOfCompany() {
        
    }
    
    public func renderData() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 2) {
                self.homeTableView.reloadData()
                self.companyResultCountLabel.isHidden = false
                self.companyResultCountLabel.text = String(format: "%02d", self.controller?.companies.count ?? 0) + " resultados encontrados"
            }
        }
    }
}

extension HomeView: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.controller?.companies.count ?? 0
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: HomeTableViewCell.identifier) as! HomeTableViewCell
        
        if let company = self.controller?.companies[indexPath.row] {
            cell.setupCell(name: company.name, photo: company.photo)
        }
        
        return cell
    }
}

extension HomeView: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let company = self.controller?.companies[indexPath.row] {
            self.controller?.presentDetailsOf(company: company)
        }
    }
}

extension HomeView: UISearchBarDelegate {
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        if let text = searchBar.text {
            self.controller?.requestListOfCompanies(with: text)
        }
    }
}
