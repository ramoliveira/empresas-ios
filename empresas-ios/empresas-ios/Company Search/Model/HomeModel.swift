//
//  HomeModel.swift
//  empresas-ios
//
//  Created by Ramon Dias de Oliveira de Almeida on 03/01/21.
//

import Foundation
import UIKit

public struct HomeModel {
    weak var controller: HomeViewController?
    
    init(controller: HomeViewController) {
        self.controller = controller
    }
    
    public func requestListOfCompanies(with name: String) {
        if let accessToken = Storage<String>.retrieve(key: Keys.accessToken.rawValue),
           let client = Storage<String>.retrieve(key: Keys.client.rawValue),
           let uid = Storage<String>.retrieve(key: Keys.uid.rawValue) {
            
            let header = [
                Keys.accessToken.rawValue:accessToken,
                Keys.client.rawValue:client,
                Keys.uid.rawValue:uid
            ]
            
            if let url = Paths.castToUrl(urlString: Paths.enterprise(by: name)) {
                Network.get(url: url, header: header) { result in
                    switch result {
                    case .failure(_):
                        self.controller?.failedRequest()
                    case .success(let data):
                        let companies = self.map(data: data.body)
                        self.controller?.successfulRequest(companies: companies)
                    }
                }
            }
        } else {
            self.controller?.failedRequest()
        }
    }
    
    public func map(data: [AnyHashable:Any]) -> [Company] {
        var output: [Company] = []
        
        if let enterprises = data["enterprises"] as? [[String:Any]] {
            for enterprise in enterprises {
                if let id = enterprise["id"] as? Int, let photoUrl = enterprise["photo"] as? String, let description = enterprise["description"] as? String, let name = enterprise["enterprise_name"] as? String {
                    
                    guard let url = Paths.castToUrl(urlString: Paths.enterprise(photo: photoUrl)), let data = try? Data(contentsOf: url), let photo = UIImage(data: data) else {
                        continue
                    }
                    
                    output.append(Company(id: id, photo: photo, description: description, name: name))
                }
            }
            return output
        }
        return output
    }
}
