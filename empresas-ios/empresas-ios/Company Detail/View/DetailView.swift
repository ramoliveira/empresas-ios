//
//  DetailView.swift
//  empresas-ios
//
//  Created by Ramon Dias de Oliveira de Almeida on 03/01/21.
//

import UIKit

public class DetailView: UIView {

    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var detailTextView: UITextView!
    
    public var controller: DetailViewController? {
        get {
            return self.parentViewController as? DetailViewController
        }
    }
    
    @IBAction func clickedBackToHome(_ sender: Any) {
        self.controller?.returnToHome()
    }
}

extension DetailView {
    public func setupView(name: String, photo: UIImage, description: String) {
        self.navigationBar.topItem?.title = name
        self.detailImageView.image = photo
        self.detailTextView.text = description
    }
}
