//
//  DetailViewController.swift
//  empresas-ios
//
//  Created by Ramon Dias de Oliveira de Almeida on 03/01/21.
//

import UIKit

public class DetailViewController: UIViewController {
    
    public var company = Company()
    
    public var detailView: DetailView! {
        guard isViewLoaded else { return nil }
        let detailView = view as! DetailView
        return detailView
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        self.detailView.setupView(name: company.name, photo: company.photo, description: company.description)
    }
}

extension DetailViewController {
    public func returnToHome() {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
    }
}
