//
//  Storage.swift
//  empresas-ios
//
//  Created by Ramon Dias de Oliveira de Almeida on 03/01/21.
//

import Foundation

public struct Storage<T> {
    public static var defaults: UserDefaults {
        get {
            return UserDefaults.standard
        }
    }
    
    public static func save(key: String, value: T) {
        Storage.defaults.setValue(value, forKey: key)
    }
    
    public static func retrieve(key: String) -> T? {
        return Storage.defaults.object(forKey: key) as? T
    }
    
}
