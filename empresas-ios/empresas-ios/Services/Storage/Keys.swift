//
//  Keys.swift
//  empresas-ios
//
//  Created by Ramon Dias de Oliveira de Almeida on 03/01/21.
//

import Foundation

public enum Keys: String {
    case accessToken = "access-token"
    case uid = "uid"
    case client = "client"
}
