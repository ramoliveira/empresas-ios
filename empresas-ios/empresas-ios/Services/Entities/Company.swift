//
//  Company.swift
//  empresas-ios
//
//  Created by Ramon Dias de Oliveira de Almeida on 31/12/20.
//

import Foundation
import UIKit

public struct Company {
    var id: Int
    var photo: UIImage
    var description: String
    var name: String
    
    init() {
        self.id = 0
        self.photo = UIImage()
        self.description = ""
        self.name = ""
    }
    
    init(id: Int, photo: UIImage, description: String, name: String) {
        self.id = id
        self.photo = photo
        self.description = description
        self.name = name
    }
}
