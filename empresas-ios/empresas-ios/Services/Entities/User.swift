//
//  User.swift
//  empresas-ios
//
//  Created by Ramon Dias de Oliveira de Almeida on 31/12/20.
//

import Foundation

public struct User: Codable {
    public var email: String = ""
    public var password: String = ""
    
    enum CodingKeys: String, CodingKey {
        case email
        case password
    }
    
    public init() {
        self.email = ""
        self.password = ""
    }
    
    public init(email: String, password: String) {
        self.email = email
        self.password = password
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        if let email = try? values.decode(String.self, forKey: .email) {
            self.email = email
        }
        if let password = try? values.decode(String.self, forKey: .password) {
            self.password = password
        }
    }
}
