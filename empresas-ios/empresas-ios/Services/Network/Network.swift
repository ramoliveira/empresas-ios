//
//  Service.swift
//  empresas-ios
//
//  Created by Ramon Dias de Oliveira de Almeida on 03/01/21.
//

import Foundation

public struct Network {
    public enum NetworkError: Error {
        case urlError
        case invalidResponse
        case serverError
        case invalidData
        case failedDecode
        case failedEncode
    }
    
    public enum NetworkRequestType: String {
        case GET = "GET"
        case POST = "POST"
    }
    
    
    public static func get(url: URL, header: [String:Any], completion: @escaping (Result<NetworkResponse<[AnyHashable:Any]>,Error>)->Void) {
        var request = URLRequest(url: url)
        request.httpMethod = NetworkRequestType.GET.rawValue
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        for (key, value) in header {
            if let value = value as? String {
                request.setValue(value, forHTTPHeaderField: key)
            } else {
                request.setValue("\(value)", forHTTPHeaderField: key)
            }
        }
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let response = response as? HTTPURLResponse else {
                completion(.failure(NetworkError.invalidResponse))
                return
            }
            
            if 200...299 ~= response.statusCode {
                if let data = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [AnyHashable:AnyObject]
                        
                        let serviceResponse = NetworkResponse<[AnyHashable:Any]>(header: response.allHeaderFields, body: json)
                        
                        completion(.success(serviceResponse))
                    } catch {
                        completion(.failure(NetworkError.failedDecode))
                    }
                } else {
                    completion(.failure(NetworkError.invalidData))
                }
            } else {
                completion(.failure(NetworkError.serverError))
            }
        }
        task.resume()
    }
    
    public static func post<T: Codable>(url: URL, body: T, ofType: T.Type, header: [String:Any], completion: @escaping (Result<NetworkResponse<[AnyHashable:Any]>,Error>)->Void) {
        var request = URLRequest(url: url)
        request.httpMethod = NetworkRequestType.POST.rawValue
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        for (key, value) in header {
            if let value = value as? String {
                request.setValue(value, forHTTPHeaderField: key)
            } else {
                request.setValue("\(value)", forHTTPHeaderField: key)
            }
        }
        
        if let requestBody = try? JSONEncoder().encode(body) {
            request.httpBody = requestBody
        } else {
            completion(.failure(NetworkError.failedEncode))
        }
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let response = response as? HTTPURLResponse else {
                completion(.failure(NetworkError.invalidResponse))
                return
            }
            
            if 200...299 ~= response.statusCode {
                if let data = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [AnyHashable:AnyObject]
                        
                        let serviceResponse = NetworkResponse<[AnyHashable:Any]>(header: response.allHeaderFields, body: json)
                        
                        completion(.success(serviceResponse))
                    } catch {
                        completion(.failure(NetworkError.failedDecode))
                    }
                } else {
                    completion(.failure(NetworkError.invalidData))
                }
            } else {
                completion(.failure(NetworkError.serverError))
            }
        }
        task.resume()
    }
}
