//
//  DefaultURL.swift
//  empresas-ios
//
//  Created by Ramon Dias de Oliveira de Almeida on 03/01/21.
//

import Foundation

public struct Paths {
    static let login = "https://empresas.ioasys.com.br/api/v1/users/auth/sign_in"
    
    static func enterpriseIndex(type: Int, name: String) -> String {
        return "https://empresas.ioasys.com.br/api/v1/enterprises?enterprise_types=\(type)&name=\(name)"
    }
    
    static func enterprise(by name: String) -> String {
        return "https://empresas.ioasys.com.br/api/v1/enterprises?name=\(name)"
    }
    
    static func enterprise(photo: String) -> String {
        return "https://empresas.ioasys.com.br\(photo)"
    }
    
    static func show(enterprise type: Int) -> String {
        return "https://empresas.ioasys.com.br/api/v1/enterprises/\(type)"
    }
    
    static func castToUrl(urlString: String) -> URL? {
        guard let url = URL(string: urlString) else {
            return nil
        }
        return url
    }
}
