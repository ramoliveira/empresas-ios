//
//  ServiceResponse.swift
//  empresas-ios
//
//  Created by Ramon Dias de Oliveira de Almeida on 03/01/21.
//

import Foundation

public struct NetworkResponse<T> {
    public var header: [AnyHashable:Any]
    public var body: T
}
