//
//  StoryboardReference.swift
//  empresas-ios
//
//  Created by Ramon Dias de Oliveira de Almeida on 03/01/21.
//

import Foundation

public enum SeguesId: String {
    case home = "goToHome"
}
